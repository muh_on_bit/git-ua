import React, { Component } from 'react';
import axios from 'axios';

import './FullPost.css';

class FullPost extends Component {
    constructor() {
        super();
        this.firstComp = React.createRef();
    }
    state = {
        Repos: [],
        Followers: [],
        Counter: 0,
        prevComp: null
    }
    componentDidMount() {
        this.loadData("/users/" + this.props.match.params.id + "/repos",
            '/users/' + this.props.match.params.id + "/followers");
    }

    componentWillUnmount() {
        this.setState({ Repos: [] });
    }

    parse_link_header(header) {
        if (header === undefined || header.length === 0) {
            return null;
        }

        var parts = header.split(',');
        var links = {};

        for (var i = 0; i < parts.length; i++) {
            var section = parts[i].split(';');
            if (section.length !== 2) {
                return null
            }
            var url = section[0].replace(/<(.*)>/, '$1').trim();
            var name = section[1].replace(/rel="(.*)"/, '$1').trim();
            links[name] = url;
        }
        return links;
    }

    loadData(repoUrl, followerUrl) {
        if (this.props.match.params.id && (repoUrl || followerUrl) && this.state.Counter < 5) {
            this.setState((prevState) => {
                return { Counter: prevState.Counter + 1 }
            })
            var repoRes, followerRes
            if (repoUrl) {
                axios.get(repoUrl)
                    .then(response => {
                        repoRes = response.headers.link;
                        repoRes = this.parse_link_header(repoRes);
                        repoRes = repoRes ? repoRes.next : null
                        const repos = response.data;
                        const updatedRepos = repos.map((repo) => {
                            return {
                                ...repo,
                            }
                        })
                        this.setState((prevState) => {
                            return { Repos: prevState.Repos.concat(updatedRepos) }
                        });
                        this.loadData(repoRes !== undefined ? repoRes : null, null)
                    });
            }
            if (followerUrl) {
                axios.get(followerUrl)
                    .then(response => {
                        followerRes = response.headers.link;
                        followerRes = this.parse_link_header(followerRes);
                        followerRes = followerRes ? followerRes.next : null
                        const followers = response.data;
                        const updatedFollowers = followers.map((follower) => {
                            return {
                                ...follower,
                            }
                        })
                        this.setState((prevState) => {
                            return { Followers: prevState.Followers.concat(updatedFollowers) }
                        });
                        this.loadData(null, followerRes !== undefined ? followerRes : null)
                    });
            }
        }
    }

    render() {
        let post = <p style={{ textAlign: 'center' }}>Please select a Post!</p>;
        if (this.props.match.params.id) {
            post = <p style={{ textAlign: 'center' }}>Loading...!</p>;
        }
        if (this.state.Repos) {
            post = (
                <div className="FullPost">
                    <div className="Heading">
                        <img className="ProfImg" src={"https://avatars.githubusercontent.com/"+this.props.match.params.id} alt = {this.props.match.params.id + " dp"} />
                        <h1>{this.props.match.params.id}</h1>
                    </div>
                    <div className="tab">
                        <button className="tablinks" name = "Repo" 
                        ref={this.firstComp}
                        onClick={(e) => {
                            if(this.state.prevComp){
                                let {prevComp} = this.state;
                                document.getElementById(this.state.prevComp.name).style.display = "none";
                                prevComp.style.backgroundColor = "#fff";
                            }
                            document.getElementById(e.target.name).style.display = "block";
                            e.target.style.backgroundColor = "#eee";  

                            this.setState({prevComp:e.target})  
                        }}>Repository List</button>

                        <button className="tablinks" name = "Followers" 
                        onClick={(e) => {
                            this.firstComp.current.style.backgroundColor = "#fff";
                            document.getElementById(this.firstComp.current.name).style.display = "none";
                            document.getElementById(e.target.name).style.display = "block";
                            e.target.style.backgroundColor = "#eee";
                            this.setState({prevComp:e.target})
                        }}>Followers</button>
                    </div>
                    <div id="Repo" className="tabcontent">
                        <h3>Repository List:</h3>
                        <br/>
                        <ul style = {{textAlign: "left"}}>
                            {this.state.Repos.map((repo) =>{
                                return (
                                        <li 
                                            key={repo.id}
                                            style = {{display: "block", marginBottom:"30px"}}>
                                            <p><strong>{repo.name}</strong></p>
                                            <a href = {repo.html_url}> {repo.html_url} </a>
                                        </li>
                                        )})
                            }
                        </ul>
                    </div>

                    <div id="Followers" className="tabcontent">
                        <h3>Followers:</h3> 
                        <br />
                        <ul style = {{textAlign: "left"}}>
                        {this.state.Followers.map((follower) =>{
                            return (
                                    <li 
                                        key = {follower.login}
                                        style = {{display: "block"}}>
                                        <p><span role = "img" aria-label="Bullet">&#x26AB;</span> <strong>{follower.login}</strong></p>
                                    </li>
                                )
                        })}
                        </ul>
                    </div>
                </div>

            );
        }
        return post;
    }
}

export default FullPost;