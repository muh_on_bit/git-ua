import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import './Blog.css';
import FullPost from './FullPost/FullPost';
import Blog from './Blog.js'

class HOC extends Component {

    constructor(props) {
        super(props);
        this.userLogin = React.createRef();
    }

    handleSearch = () => {
        if (this.userLogin) {
            console.log(this)
        }
    }

    render() {
        return (
            <div className="Blog">
                <Switch>
                    <Route path="/users" exact component={Blog} />
                    <Route path={`/users/:id`} exact component={FullPost} />
                    <Redirect from="/" to="/users" />
                </Switch>
            </div>
        );
    }
}

export default HOC;