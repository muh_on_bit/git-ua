import React, { Component } from 'react';
import axios from '../../../axios';
import Post from '../../../components/Post/Post';
import './Posts.css';

class Posts extends Component {

    constructor() {
        super();
        this.myRef = React.createRef();
    }

    state = {
        posts: [],
        error: true,
        link: "/users",
        scrolling: false
    }

    parse_link_header(header) {
        if (header === undefined || header.length === 0) {
            return null;
        }

        var parts = header.split(',');
        var links = {};

        for (var i = 0; i < parts.length; i++) {
            var section = parts[i].split(';');
            if (section.length !== 2) {
                return null
            }
            var url = section[0].replace(/<(.*)>/, '$1').trim();
            var name = section[1].replace(/rel="(.*)"/, '$1').trim();
            links[name] = url;
        }
        return links;
    }

    loadUsers() {
        if (this.state.link) {
            this.setState({ scrolling: true });
            var { link } = this.state;
            var userRes;
            axios.get(link)
                .then(response => {
                    const posts = response.data;
                    userRes = response.headers.link;
                    userRes = this.parse_link_header(userRes)
                    userRes = userRes ? userRes.next : null
                    const updatedPosts = posts.map(post => {
                        return {
                            ...post,
                        }
                    });

                    this.setState((prevState) => {
                        return { posts: prevState.posts.concat(updatedPosts) }
                    })
                    this.setState({ link: userRes, error: false, scrolling: false });
                })
                .catch(error => {
                    this.setState({ error: true, scrolling: true })
                });
        }
    }

    componentDidMount() {
        this.loadUsers();
        this.setState({ scrolling: false });
        this.scrollListener = window.addEventListener('scroll', () => {
            this.handleScroll();
        })
    }

    postSelectedHandler = (id) => {
        this.props.history.push("/users/" + id);
        this.setState({ scrolling: true });
    }

    handleScroll = () => {
        if (this.state.scrolling || !this.myRef.current) return;
        console.log(this.myRef);
        const lastEl = this.myRef.current;
        const lastElOffset = lastEl.offsetTop + lastEl.clientHeight;
        const pageOffset = window.pageYOffset + window.innerHeight;
        var bottomOffset = 10;

        if (pageOffset > lastElOffset - bottomOffset) this.loadUsers();
    }

    render() {
        let posts;
        if (!this.state.error) {
            posts = this.state.posts.map(post => {
                return (
                    <div ref = {this.myRef} key = { post.id }>
                        <Post
                            login={post.login}
                            profileimg={post.avatar_url}
                            clicked={() => this.postSelectedHandler( post.login )} />
                    </div>
                );
            });
        }

        return (
            <div className = "MainPage">
                <section className="Posts">
                    {posts}
                </section>
                <div className="lds-ring" onClick = {() => this.loadUsers()}><div></div><div></div><div></div><div></div></div>
            </div>
        );
    }
}

export default Posts;