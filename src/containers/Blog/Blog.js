import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import './Blog.css';
import Posts from './Posts/Posts';

class Blog extends Component {

    constructor(props) {
        super(props);
        this.userLogin = React.createRef();
    }

    handleSearch = () => {
        if (this.userLogin) {
            this.props.history.push("/users/" + this.userLogin.current.value);
        }
    }

    render() {
        return (
            <div className="Blog">
                <header>
                    <div className="wrap">
                        <div className="search">
                            <input type="text" ref = {this.userLogin} className="searchTerm" placeholder="Enter User's Login" />
                                <button type="submit" className="searchButton" onClick = {this.handleSearch}>
                                    <i className="fa fa-search" />
                                </button>
                        </div>
                    </div>
                </header>
                <Route path="/users" exact component={Posts} />
            </div>
        );
    }
}

export default Blog;