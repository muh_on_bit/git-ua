import React from 'react';

import './Post.css';

const post = (props) => (
    <article className="Post" onClick={props.clicked}>
        <h1>{props.login}</h1>
        <img className="Img" src={"https://avatars.githubusercontent.com/"+props.login} alt = {props.login + " dp"} />
    </article>
);

export default post;